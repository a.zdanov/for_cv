# Change logs:
# 0.4: меняем интервал с 8 недель до 4 и не выкидываем крайние значения
# 0.5: сделать анализ ТОП N страниц, не убирать из мониторинга их, если они без алертов
# 0.6: разделит зоны по разным таблицам, добавить отдельные таблицы ТОП N страниц
# 0.7 выделил хеплдеск в отдельную таблицу
# 0.8 попросили разнести RU и вернуть WEST в разные сообщения
# 0.9 разносим RU и WEST в разные группы
# 0.10 для RU выделить Yandex и Google, добавить ключевые таблицы
# 0.11 для WEST почистить левые hostname и страницы без трафика
# 0.12 вернуть лимит для всех страниц, по дефолту 40 сессий

# 0.13
# нашел ошибку, что без pagetoken выгружаются не все записи
# оптимизация с использованием np.where
# RU просят вернуть все страницы
# выгружаем 3 medium: organic, none and referral для поиска amp страниц для WEST

import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from oauth2client.service_account import ServiceAccountCredentials
from apiclient.discovery import build
from apiclient.errors import HttpError
import socket
import requests
from statistics import fmean  # для расчета среднего значения в определении аномалий по v2
from urllib.parse import urlparse
import params as prm

socket.setdefaulttimeout(3000)

desired_width = 320
pd.set_option('display.width', desired_width)
pd.set_option('display.max_columns', 20)
pd.options.display.max_columns = 50
pd.options.display.max_rows = 50

LAST_DAYS = 28  # отсекает массив из 8 последних недель
LAST_DAY = 1  # нужно для поиска вчерашнего дня

RU_SECRET = prm.RU_SECRET
WEST_SECRET = prm.WEST_SECRET
BITRIX_URL = prm.URL

# флаг включает куда отправлять
TO_BITRIX_RU = 1
TO_BITRIX_WEST = 1
IS_SEND = 1  # 0 отключает отправку в Битрикс

DIFF_PERCENT = 0.30  # какой % мы будем считать плюс или минус от среднего
MIN_ALL_SESSIONS = 40  # сколько на каждую страницу минимум сессий ИТОГО

RU_ACCOUNTS = prm.RU_ACCOUNTS
WEST_ACCOUNTS = prm.WEST_ACCOUNTS


def remove_hostname_from_path(x):
    """
    функция убирает из пути название домена, если он еще остался
    """
    path, hostname = x['path'], x['hostname']
    result = path.replace(hostname, '')
    if '/amp/' in path:
        result = result.replace('.html', '').replace('.php', '')
    return result


def remove_columns_from_df(df):
    """
    функция убирает лишние колонки из финального датафрейма
    """
    df = df.drop(columns=['zone', 'is_alert', 'mean_value', 'up', 'low', 'last_week', 'total'])
    return df


def get_mean_per_row(x):
    """
    функция находит среднее в числовом ряду
    """
    list_metric = x.to_list()[:3]
    return fmean(list_metric)


def get_is_alert(x):
    """
    is_alert = 1 максимальная аномалия
    is_alert = 2 минимальная аномалия
    is_alert = 3 нулевая аномалия
    is_alert = 10 без аномалий
    """
    last_value = x['last_week']
    up = x['up']
    low = x['low']
    if last_value == 0:
        is_alert = 3
    elif last_value > up:
        is_alert = 1
    elif last_value < low:
        is_alert = 2
    else:
        is_alert = 10
    return is_alert


def check_anomaly(df):
    """
    функция ищет аномалии в данных по следующему алгоритму:
    - значение плюс или минус DIFF_PERCENT от среднего будет аномалией
    """
    df["mean_value"] = df.apply(get_mean_per_row, axis=1)
    df['up'] = df["mean_value"] * (1 + DIFF_PERCENT)
    df["low"] = df["mean_value"] * (1 - DIFF_PERCENT)
    df["is_alert"] = df.apply(get_is_alert, axis=1)
    return df


def run_seo_alerts(region, accounts):
    metric = 'sessions'
    today = date.today()
    yesterday = today - timedelta(days=1)
    yesterday_str = f'{yesterday.strftime("%m/%d")}'
    text = f'\n<h2>Organic сессии на {yesterday_str}</h2>'  # сюда пишем данные для отправки в Битрикс

    data = get_ga(region, accounts)
    data['scheme'], data['netloc'], data['path'], data['params'], data['query'], data['fragment'] = \
        zip(*data['landingPagePath'].map(urlparse))
    data['path'] = data.apply(remove_hostname_from_path, axis=1)
    # для ru делаем детализацию с source и только yandex и google
    if region == 'west':
        data["is_amp"] = np.where(data['path'].str.contains('/amp/'), 1, 0)
        data = data.query('is_amp == 1  or medium == "organic"')
        data = data[['date', 'zone', 'path', 'hostname', 'device', metric]] \
            .groupby(by=['date', 'zone', 'path', 'hostname', 'device']).sum()
    else:
        data = data.query('source in ("yandex", "google")')
        data = data[['date', 'zone', 'path', 'hostname', 'source', metric]] \
            .groupby(by=['date', 'zone', 'path', 'hostname', 'source']).sum()

    zones = data.reset_index().zone.drop_duplicates().to_list()

    for zone in zones:
        data_temp = data.query(f"zone == '{zone}'").copy().reset_index()
        # уберем случайные hostname
        data_temp = data_temp[data_temp['hostname'].str.contains("bitrix") == True]
        data_temp = data_temp[data_temp['hostname'].str.contains(".site") == False]
        data_temp = data_temp[data_temp['hostname'].str.contains(".shop") == False]
        data_temp = data_temp[data_temp['hostname'].str.contains(".translate.") == False]
        data_temp = data_temp[data_temp['hostname'].str.contains("partners.bitrix24.") == False]
        if region == 'west':
            data_temp = data_temp.pivot_table(index=['zone', 'hostname', 'path', 'device'],
                                              columns=['date'], values='sessions', aggfunc='sum',
                                              fill_value=0, margins=True, margins_name='total')
        else:
            data_temp = data_temp.pivot_table(index=['zone', 'hostname', 'path', 'source'],
                                              columns=['date'], values='sessions', aggfunc='sum',
                                              fill_value=0, margins=True, margins_name='total')
        df = data_temp.query(f'total > {MIN_ALL_SESSIONS}  and zone != "total"').copy()

        if len(df) > 0:
            text = text + f'\n<h3>Зона {(zone.upper())}</h3>'
            # сделаем дубль столбца последней недели для использования в расчетах, потом удалим
            df['last_week'] = df.iloc[:, -2:-1]

            df = check_anomaly(df)
            df['status'] = df.apply(get_bitrix_status, axis=1)
            df = df.sort_values(['zone', 'hostname', 'path'])

            if region == 'west':
                top_pages = ('/', '/apps/desktop.php', '/prices/', '/self-hosted/download.php', '/tools/crm/',
                             '/applications.php', '/partners/')
            else:
                top_pages = ('/', '/tasks/', '/about/agreement.php', '/apps/', '/apps/mobile-and-desktop-apps.php',
                             '/articles/crm_what_is.php', '/auth/', '/authorization/', '/features/',
                             '/features/box/box.php', '/features/crm/', '/features/desktop.php',
                             '/features/pages/training.php', '/features/sites.php', '/features/tasks.php',
                             '/partners/', '/prices/', '/prices/self-hosted.php', '/whatisthis/')
            top_hostname = 'auth2.bitrix24.net'

            # разделим df на 2 части без helpdesk и c helpdesk
            # df_wo_helpdesk = df.query(f'"helpdesk" not in hostname').copy().reset_index()
            df_wo_helpdesk = df.copy().reset_index()
            df_wo_helpdesk = df_wo_helpdesk[df_wo_helpdesk['hostname'].str.contains("helpdesk") == False]

            # разделим данные без helpdesk еще на 2 части: top_pages и все остальные
            df_top = df_wo_helpdesk.query(f"(path in {top_pages} and hostname != '{top_hostname}')"
                                          f"or (hostname == '{top_hostname}' and path == '/')").copy()
            df_top = remove_columns_from_df(df_top)
            html_top = df_top.to_html(classes='data-table', escape=False, index=False, index_names=False)
            text = text + f'<\nfont size="2" >'
            text = text + '\n<p>Мониторинг важных страниц:</p>'
            text = text + f'\n{html_top}'

            df_anomaly = df_wo_helpdesk.query(f'is_alert < 10 and path not in {top_pages}'
                                              f'and hostname != "{top_hostname}"') \
                .copy()

            if len(df_anomaly):
                text = text + '\n<p>Аномалии в трафике:</p>'
                df_anomaly = remove_columns_from_df(df_anomaly)
                html_anomaly = df_anomaly.to_html(classes='data-table', escape=False, index=False,
                                                  index_names=False)
                text = text + f'\n{html_anomaly}'

            df_helpdesk = df.query(f'is_alert < 10 and hostname == "helpdesk.bitrix24.ru"').copy().reset_index()
            if len(df_helpdesk):
                text = text + '\n<p>Аномалии в helpdesk:</p>'
                df_helpdesk = remove_columns_from_df(df_helpdesk)
                html_helpdesk = df_helpdesk.to_html(classes='data-table', escape=False, index=False,
                                                    index_names=False)
                text = text + f'\n{html_helpdesk}'

    head_bitrix_msg = generate_bitrix_head_msg(metric)
    return head_bitrix_msg, text


def get_bitrix_status(x):
    is_alert = x['is_alert']
    curr_value = x['last_week']
    avg_value = x['mean_value']
    if avg_value == 0:
        diff_percent = 100
    else:
        diff_percent = int((curr_value - avg_value) * 100 / avg_value)

    if is_alert == 1 and avg_value > 0:  # зеленый
        bitrix_status = '<font color="#32CD32">Выросли на ' + f'{diff_percent}%' + '</font>'
    elif is_alert == 1 and avg_value == 0:  # зеленый
        bitrix_status = '<font color="#32CD32">Пошел траффик' + '</font>'
    elif is_alert == 2:  # желтый
        bitrix_status = '<font color="#B8860B">Упали на ' + f'{diff_percent}%' + '</font>'
    elif is_alert == 3:  # красный
        bitrix_status = '<font color="#EE4B2B">Стоп траффик' + '</font>'
    elif is_alert == 10:
        if diff_percent > 1:
            bitrix_status = '<font color="#32CD32">Выросли на ' + f'{diff_percent}%' + '</font>'
        elif diff_percent < -1:
            bitrix_status = '<font color="#B8860B">Упали на ' + f'{diff_percent}%' + '</font>'
        else:
            bitrix_status = 'Без изменений'
    return bitrix_status


def generate_bitrix_head_msg(metric):
    today = date.today()
    yesterday = today - timedelta(days=1)
    msg = f'''{yesterday.strftime("%m/%d")} SEO {metric.upper()} # аномалии'''
    return msg


def send_to_bitrix(secret, head_msg, text):
    text_end = f'\n<p>Сравнение осуществляется относительно средних значений за последние 4 недели. Порог определения ' \
               f'аномалии +-{int(DIFF_PERCENT * 100)}%.</p> '
    text = text + text_end
    data = {
        'hash': secret,
        'title': head_msg,
        'message': text
    }
    headers = {
        "Content-type": "application/x-www-form-urlencoded",
        "Accept": "text/plain",
    }
    requests.post(BITRIX_URL, headers=headers, data=data)


def generate_date_range():
    start_date = (datetime.now() - timedelta(days=LAST_DAY)).date()
    date_list = []
    for day in range(LAST_DAYS):
        a_date = (start_date - timedelta(days=day))
        if a_date.weekday() == start_date.weekday():
            date_list.append(a_date.isoformat())

    return date_list[:4]


################################################################################
# технический блок функция для выгрузки из Google Analytics
def initialize_analyticsreporting():
    scopes = ['https://www.googleapis.com/auth/analytics.readonly']
    key_file_location = 'service_account_bitrix.json'

    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        key_file_location, scopes)
    analytics = build('analyticsreporting', 'v4', credentials=credentials)
    return analytics


def get_report(analytics, date, ga_id, region, pageToken=None):
    """
    функция задает метрики и дименшены для выгрузки из GA
    :param analytics: параметры подключения к GA
    :param date: дата выгрузки, выгружаем 1 день
    :param ga_id: счетчик
    :param region: тип региона
    :param pageToken: pageToken на случай больших данных
    :return:
    """
    if region == 'west':
        body = {'reportRequests': [{
            'viewId': ga_id,
            'pageSize': 10000,
            'pageToken': pageToken,
            'dateRanges': [{'startDate': date, 'endDate': date}],
            'metrics': [{'expression': 'ga:sessions'}],
            'dimensions': [{'name': 'ga:date'}, {'name': 'ga:landingPagePath'}, {'name': 'ga:countryIsoCode'},
                           {'name': 'ga:hostname'}, {'name': 'ga:deviceCategory'}, {'name': 'ga:source'},
                           {'name': 'ga:medium'}],
            'dimensionFilterClauses': [
                {'filters': [
                    {'dimension_name': 'ga:medium', 'operator': 'IN_LIST',
                     'expressions': ["organic", "referral", "(none)"]}
                ]}
            ]
        }]}
    else:
        body = {'reportRequests': [{
            'viewId': ga_id,
            'pageSize': 10000,
            'pageToken': pageToken,
            'dateRanges': [{'startDate': date, 'endDate': date}],
            'metrics': [{'expression': 'ga:sessions'}],
            'dimensions': [{'name': 'ga:date'}, {'name': 'ga:landingPagePath'}, {'name': 'ga:countryIsoCode'},
                           {'name': 'ga:hostname'}, {'name': 'ga:deviceCategory'}, {'name': 'ga:source'},
                           {'name': 'ga:medium'}],
            'dimensionFilterClauses': [
                {'filters': [
                    {'dimension_name': 'ga:medium', 'operator': 'EXACT', 'expressions': 'organic'},
                    {'dimension_name': 'ga:sourceMedium', 'operator': 'EXACT', 'expressions': 'yandex.ru / referral'}

                ]}
            ]
        }]}

    while True:
        try:
            return analytics.reports().batchGet(body=body).execute()
        except HttpError:
            continue
        break


def format_summary(response):
    """
    типовая функция из мануала API GA
    :param response: получает ответ из GA
    :return: возвращает датафрейм pandas
    """
    try:
        # create row index
        try:
            row_index_names = response['reports'][0]['columnHeader']['dimensions']
            row_index = [element['dimensions'] for element in response['reports'][0]['data']['rows']]
            row_index_named = pd.MultiIndex.from_arrays(np.transpose(np.array(row_index)),
                                                        names=np.array(row_index_names))
        except:
            row_index_named = None

        # extract column names
        summary_column_names = [item['name'] for item in response['reports'][0]
        ['columnHeader']['metricHeader']['metricHeaderEntries']]

        # extract table values
        summary_values = [element['metrics'][0]['values'] for element in response['reports'][0]['data']['rows']]

        # combine. I used type 'float' because default is object, and as far as I know, all values are numeric
        df = pd.DataFrame(data=np.array(summary_values),
                          index=row_index_named,
                          columns=summary_column_names).astype('float')
    except:
        df = pd.DataFrame()

    return df


def format_pivot(response):
    """
    типовая функция из мануала API GA
    """
    try:
        # extract table values
        pivot_values = [item['metrics'][0]['pivotValueRegions'][0]['values'] for item in response['reports'][0]
        ['data']['rows']]

        # create column index
        top_header = [item['dimensionValues'] for item in response['reports'][0]
        ['columnHeader']['metricHeader']['pivotHeaders'][0]['pivotHeaderEntries']]
        column_metrics = [item['metric']['name'] for item in response['reports'][0]
        ['columnHeader']['metricHeader']['pivotHeaders'][0]['pivotHeaderEntries']]
        array = np.concatenate((np.array(top_header),
                                np.array(column_metrics).reshape((len(column_metrics), 1))),
                               axis=1)
        column_index = pd.MultiIndex.from_arrays(np.transpose(array))

        # create row index
        try:
            row_index_names = response['reports'][0]['columnHeader']['dimensions']
            row_index = [element['dimensions'] for element in response['reports'][0]['data']['rows']]
            row_index_named = pd.MultiIndex.from_arrays(np.transpose(np.array(row_index)),
                                                        names=np.array(row_index_names))
        except:
            row_index_named = None
        # combine into a dataframe
        df = pd.DataFrame(data=np.array(pivot_values),
                          index=row_index_named,
                          columns=column_index).astype('float')
    except:
        df = pd.DataFrame()
    return df


def format_report(response):
    """
    типовая функция из мануала API GA
    """
    summary = format_summary(response)
    pivot = format_pivot(response)
    if pivot.columns.nlevels == 2:
        summary.columns = [[''] * len(summary.columns), summary.columns]

    return pd.concat([summary, pivot], axis=1)


def get_ga(region, accounts):
    """
    основная функция по выгрузке данных из GA
    по каждому счетчику из набора дат проходим и выгружаем данные
    складываем все в один датафрейм pandas
    т.к. набор данных небольшой, то проходим по 1 дню, без детализаций в часы
    :param date_list: набор дат для выгрузки
    :return: датафрейм с данными
    """
    date_list = generate_date_range()
    count = 0
    for account in accounts:
        ga_id = account["ga_id"]
        zone = account["zone"]
        print(f"Load from view_id = {ga_id}, zone = {zone}")
        for date in date_list:
            print(date)
            analytics = initialize_analyticsreporting()
            response = get_report(analytics, date, ga_id, region)
            pageToken = response['reports'][0].get('nextPageToken')
            # convert the report into pandas dataframe
            if count == 0:
                result = format_report(response)
                result["ga_id"] = ga_id
                result["zone"] = zone
            else:
                temp = format_report(response)
                temp["ga_id"] = ga_id
                temp["zone"] = zone
                result = pd.concat([result, temp], axis=0)
            count += 1

            while pageToken is not None:
                analytics = initialize_analyticsreporting()
                response = get_report(analytics, date, ga_id, region, str(int(pageToken) + 1))
                pageToken = response['reports'][0].get('nextPageToken')  # update the pageToken
                # convert the report into pandas dataframe
                temp2 = format_report(response)
                temp2["ga_id"] = ga_id
                temp2["zone"] = zone
                result = pd.concat([result, temp2], axis=0)

    result = result.reset_index() \
        .rename(columns={'ga:date': 'date', 'ga:landingPagePath': 'landingPagePath',
                         'ga:hostname': 'hostname', 'ga:sessions': 'sessions', 'ga:source': 'source',
                         'ga:deviceCategory': 'device', 'ga:countryIsoCode': 'countryIsoCode',
                         'ga:medium': 'medium'
                         }
                )

    result['date'] = pd.to_datetime(result.date, format='%Y%m%d').dt.date
    result["device"] = np.where(result['device'] == 'tablet', 'mobile', result['device'])
    result["source"] = np.where(result['source'] == 'yandex.ru', 'yandex', result['source'])
    result.astype({'sessions': 'int32'})

    conditions = [(result['zone'] == 'com') & (result['countryIsoCode'] == 'US'),
                  (result['zone'] == 'com'),
                  (result['zone'] == 'cn_tc'),
                  (result['zone'] == 'com_th'),
                  (result['zone'] == 'in_hi'),
                  (result['zone'] == 'my'),
                  (result['zone'] == 'es')]
    choices = ['us', 'en без us', 'cn', 'th', 'hi', 'ms', 'la']
    result["zone"] = np.select(conditions, choices, default=result['zone'])
    return result


def main():
    if TO_BITRIX_RU:
        head_bitrix_msg, text = run_seo_alerts('ru', RU_ACCOUNTS)
        if head_bitrix_msg != '' and IS_SEND:
            send_to_bitrix(RU_SECRET, head_bitrix_msg, text)
            print('Отправили sessions в bitrix успешно')

    if TO_BITRIX_WEST:
        head_bitrix_msg, text = run_seo_alerts('west', WEST_ACCOUNTS)
        if head_bitrix_msg != '' and IS_SEND:
            send_to_bitrix(WEST_SECRET, head_bitrix_msg, text)
            print('Отправили sessions в bitrix успешно')


if __name__ == '__main__':
    main()
