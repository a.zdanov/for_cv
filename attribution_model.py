# log version:
# начиная с 0.9 версии мы рассчитываем на расчет атрибуции на 1 день ВЧЕРА или опционально
# пересчитать любой другой день вручную
# 1.0 финальная версия с настройкой на финальную таблицу в bwh
# 1.1 убрал удаление key столбца в промежуточных таблицах, удалим в самом конце
# 2.0 крупное изменение, отказываемся от client_id_no_cid, перегрузили все сессии GA и YM с ID
# 3.0 крупное изменение, тестируем данные Google из GoogleBiqQuery для GA4
# 3.1 тестируем еще раз данные из Google для GA4 в новом формате
# 3.2 дописываю реализацию в ежедневном формате в prod
# 3.3 добавляем поле для utm разметки старой
# 3.4 возвращаем данные для словаря в CH
# 3.5 referral убираем из приоритета наравне с direct
import pandas as pd
from sqlalchemy import create_engine as ce
from datetime import date, timedelta
from clickhouse_driver import Client
import socket
import params as prm
import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)
socket.setdefaulttimeout(3000)

desired_width = 320
pd.set_option('display.width', desired_width)
pd.set_option('display.max_columns', 20)
pd.options.display.max_columns = 50
pd.options.display.max_rows = 50

MAX_DAYS = 7  # максимальный период между регой и сессией
START_DAY = 25  # нужно для поиска вчерашнего дня
MAX_DAYS_NETWORK = 180  # максимальный период для поиска данных в нетворке

settings = {
    'use_numpy': True}
CONNECTION_CH = Client(host=prm.HOST, port=prm.PORT, user=prm.USER, password=prm.PASSWORD, database=prm.DATABASE,
                       verify=False, compression='lz4', settings=settings)
CONNECTION_MYSQL = ce(prm.CE)

# флаг включает куда отправлять
TO_CH = 1
TO_FILE = 0
REG_TABLE = 'bwh.ga_traffic_py3_ga4'

# флаг 0 убирает подробные print команды
ENABLE_PRINT = 0


def get_reg_sql(reg_date):
    """
    :param reg_date: дата регистраций
    :return: dataframe, полученный как список регистраций из mySQL + добавленный к нему яндекс ID
    """
    select = f"""
           select
              bs.controller_member_id XML_ID,
              cm.name URL,
              cg.name TARIF,
              um.uf_open_date OPEN_DATE,
              um.uf_ga_cid GA_CID,
              um.uf_reg_country REG_COUNTRY,
              bs.db_name DB_NAME,
              um.UF_REG_PLATFORM REG_PLATFORM,
              um.UF_UTM_SOURCE UTM_SOURCE,
              um.UF_UTM_MEDIUM UTM_MEDIUM,
              um.UF_UTM_CAMPAIGN UTM_CAMPAIGN,
              um.UF_UTM_CONTENT UTM_CONTENT,
              um.UF_NETWORK_USER_ID NETWORK_USER_ID,              
              bs.EMAIL,
              CASE
                WHEN um.UF_UTM_MEDIUM  = 'bxtest' THEN 1
                WHEN cm.name like 'bxcreate%%' THEN 1
                WHEN cm.name like 'bxstage%%' THEN 1
                WHEN cm.name like 'bxprod%%' THEN 1
                WHEN cm.name like 'bxtest%%' THEN 1
                WHEN bs.EMAIL  like '%%mail.bx24.net%%' THEN 1
              ELSE 0
              END as is_test_domain
            from b24_sites bs
              inner join b_controller_member cm on cm.id = bs.controller_member_id
              inner join b_uts_controller_member um on um.value_id = bs.controller_member_id
              inner join b_controller_group cg on cg.id = cm.controller_group_id
            where
            status = 'finish' and
            um.uf_open_date >= '{reg_date}' and um.uf_open_date <= '{reg_date} 23:59:59'
            """
    df = pd.read_sql_query(select, CONNECTION_MYSQL)

    # получим эти же реги, но из CH - там Yandex Metric ID
    select = f"""
    select distinct db_name, ym_uid from b24stat.b24_site_reg
    where toDate(open_date) = '{reg_date}' and ym_uid != ''
    """
    result, columns = CONNECTION_CH.execute(select, with_column_types=True)
    df_ym = pd.DataFrame(result, columns=[tuple[0] for tuple in columns])

    df = pd.merge(df, df_ym, how='left', left_on=['DB_NAME'], right_on=['db_name']).drop(
        columns=['db_name'])

    df.NETWORK_USER_ID = df.NETWORK_USER_ID.astype('int64')
    # df.to_csv(f'df_regs_test.csv', index=False)
    # df = pd.read_csv('df_regs_test.csv')
    # df.NETWORK_USER_ID = df.NETWORK_USER_ID.astype('int64')
    return df


def get_ga_visits(start_date):
    """
    :param last_day:
    :param start_date:
    :return:
    """
    # возьмем все визиты за последние MAX_DAYS
    start_date_visit = start_date + timedelta(days=-MAX_DAYS)
    table = 'bwh.ga4_visits'

    select = f"""
    SELECT
    REGION AS `zone`,
    ga_id,
    country_iso AS country_iso_code,
    cid AS ga_cid,
    toDateTime(event_timestamp) AS date_hour_minute,
    source_medium,
    utm_content,
    utm_campaign,
    last_page_path AS landing_page_path,
    device AS device_category,
    session AS sessions,
    regs AS reg_goal,
    login AS login_goal
    FROM 
    (
    SELECT *
    FROM {table}
    where toDate(date) > '{start_date_visit}' and toDate(date) <= '{start_date}'
    ) visits
    LEFT OUTER JOIN b24stat.b24_region_sales AS regions ON upper(visits.country_iso) = regions.two_letter_ISO 

     """
    result, columns = CONNECTION_CH.execute(select, with_column_types=True)
    df = pd.DataFrame(result, columns=[column[0] for column in columns])
    df['date_hour_minute'] = pd.to_datetime(df['date_hour_minute'], format='%Y-%m-%d %H:%M:%S')

    df['key'] = df['date_hour_minute'].astype(str) + "@" + df['country_iso_code'] + "@" + df['source_medium'] + \
                "@" + df['utm_campaign'] + "@" + df['utm_content'] + \
                "@" + df['landing_page_path'] + "@" + df['device_category']

    return df


def get_ym_visits(start_date, last_day=1):
    # возьмем все визиты за последние MAX_DAYS
    start_date_visit = start_date + timedelta(days=-MAX_DAYS)
    # если last_day > 1, значит мы ищем не вчерашний день и его нужно искать в основной таблице
    sign = '<=' if last_day > 1 else '<'

    select = f"""
        SELECT 
        `zone`, 
        date_hour_minute, 
        ym_client_id, 
        device_category, 
        country_iso_code, 
        landing_page_path, 
        CONCAT(`source`,' / ',medium) AS source_medium,
        utm_content,
        utm_campaign,
        sessions, 
        reg_goal, 
        login_goal
        FROM b24stat.BI_sessions_ym
      where toDate(date_hour_minute) > '{start_date_visit}' and toDate(date_hour_minute) {sign} '{start_date}'
       """

    result, columns = CONNECTION_CH.execute(select, with_column_types=True)
    df = pd.DataFrame(result, columns=[column[0] for column in columns])
    df['date_hour_minute'] = pd.to_datetime(df['date_hour_minute'], format='Y-%m-%d %H:%M:%S')
    df.ym_client_id = df.ym_client_id.astype('str')
    return df


def get_bad_domain():
    df = pd.read_excel('domainsRegEmail.xlsx')
    df = df.query('type in ("bad tempmail", "bad unsafe", "bitrix")')
    return df


def get_mobile_regs(df_final, df_reg):
    df = df_reg.query('REG_PLATFORM == "mobile"').copy()
    df["type"] = "mobile"
    df["subtype"] = "mobile"
    df_final = pd.concat([df_final, df], ignore_index=True)
    if ENABLE_PRINT:
        print('\tdf_mobile:', df.shape)
    return df_final


def get_test_regs(df_final, df_reg):
    df = df_reg.query('is_test_domain == 1').copy()
    df["type"] = "test"
    df["subtype"] = "test"
    df_final = pd.concat([df_final, df], ignore_index=True)
    if ENABLE_PRINT:
        print('\tdf_test:', df.shape)
    return df_final


def get_bad_domain_regs(df_final, df_reg, df_bad_domain):
    df = df_reg.copy()
    df['email_domain'] = df['EMAIL'].str.split('@', expand=True)[1]
    df = pd.merge(
        df, df_bad_domain,
        how='inner', left_on=['email_domain'], right_on=['domain']).drop(
        columns=['legal_status', 'is_public_mail', 'origin_zone', 'service_name', 'id', 'email_domain',
                 'type', 'domain', 'industry'])
    df["type"] = "bad domain"
    df["subtype"] = "bad domain"
    df_final = pd.concat([df_final, df], ignore_index=True)
    if ENABLE_PRINT:
        print('\tdf_bad_domain:', df.shape)
    return df_final


def get_good_match(df_final, df_reg, df_visit, type_visit, no_direct):
    # для джойна оставим только реги с reg_goal = 1 и не прямые заходы (опционально)
    if no_direct == 1:
        df_visit = df_visit.query('reg_goal >= 1 and source_medium != "(direct) / (none)"')
        df_visit = df_visit.loc[~df_visit['source_medium'].str.contains(' / referral', case=False)]
    else:
        df_visit = df_visit.query('reg_goal >= 1')

    if type_visit == 'ga':
        df = df_reg.query('not GA_CID.isnull() & GA_CID != "."').query('not REG_COUNTRY.isnull()').copy()
        df = pd.merge(df, df_visit, how='inner', left_on=['GA_CID', 'REG_COUNTRY'],
                      right_on=['ga_cid', 'country_iso_code']) \
            .drop(columns=['ga_cid', 'country_iso_code'])
    elif type_visit == 'ym':
        df = df_reg.query('not ym_uid.isnull()').query('not REG_COUNTRY.isnull()').copy()
        df = pd.merge(df, df_visit, how='inner', left_on=['ym_uid', 'REG_COUNTRY'],
                      right_on=['ym_client_id', 'country_iso_code']) \
            .drop(columns=['ym_client_id', 'country_iso_code'])

    if len(df) != 0:
        # важный момент остались дубликаты, когда было несколько сессий с регистрациями в разное время
        df["reg_lag"] = abs((df.OPEN_DATE - df.date_hour_minute) / pd.Timedelta(minutes=1))
        df = df.query(f'reg_lag <= {MAX_DAYS} * 24 * 60')
        if len(df) != 0:
            df = df.sort_values('reg_lag').drop_duplicates(['DB_NAME'], keep='first')
            df["type"] = "good_match"
            df["subtype"] = f"{type_visit}_reg_goal_1_no_direct_{no_direct}"
            df_final = pd.concat([df_final, df], ignore_index=True)
    if ENABLE_PRINT:
        print(f'\tdf_good_match_{type_visit}:', df.shape)
    return df_final


def get_cid_match(df_final, df_reg, df_visit, type_visit, type_match, no_direct):
    # джойн будет только по CID (без учета страны регистрации)
    # с reg_goal = 1 (опционально) и не прямые заходы (опционально)
    if no_direct == 1:
        df_visit = df_visit.query('source_medium != "(direct) / (none)"')
        df_visit = df_visit.loc[~df_visit['source_medium'].str.contains(' / referral', case=False)]

    if type_match == 1:
        df_visit = df_visit.query('reg_goal >= 1')
    else:
        df_visit = df_visit.query('reg_goal == 0')

    if type_visit == 'ga':
        df = df_reg.query('not GA_CID.isnull() & GA_CID != "."').copy()
        df = pd.merge(df, df_visit, how='inner', left_on=['GA_CID'],
                      right_on=['ga_cid']) \
            .drop(columns=['ga_cid'])
    elif type_visit == 'ym':
        df = df_reg.query('not ym_uid.isnull()').copy()
        df = pd.merge(df, df_visit, how='inner', left_on=['ym_uid'],
                      right_on=['ym_client_id']) \
            .drop(columns=['ym_client_id'])

    if len(df) != 0:
        # важный момент остались дубликаты, когда было несколько сессий с регистрациями в разное время
        df["reg_lag"] = abs((df.OPEN_DATE - df.date_hour_minute) / pd.Timedelta(minutes=1))
        df = df.query(f'reg_lag <= {MAX_DAYS} * 24 * 60')
        if len(df) != 0:
            df = df.sort_values('reg_lag').drop_duplicates(['DB_NAME'], keep='first')
            df["type"] = "cid_match"
            df["subtype"] = f"{type_visit}_reg_goal_{type_match}_no_direct_{no_direct}"
            df_final = pd.concat([df_final, df], ignore_index=True)

    if ENABLE_PRINT:
        print(f'\tdf_cid_match_{type_visit}_{type_match}:', df.shape)
    return df_final


def get_networkUserId_regs(start_date, df_final, df_reg):
    # возьмем все визиты за последние MAX_DAYS
    start_date_network = start_date + timedelta(days=-MAX_DAYS_NETWORK)

    # важно не включать последний день в период!
    select = f"""
  SELECT 
    networkUserId, 
    argMax(db_name, OPEN_DATE) AS db_name,
    argMax(ga_cid, OPEN_DATE) AS ga_cid,
    argMax(source_medium, OPEN_DATE) AS source_medium,
    argMax(utm_campaign, OPEN_DATE) AS utm_campaign,
    argMax(utm_content, OPEN_DATE) AS utm_content,
    argMax(landing_page_path, OPEN_DATE) AS landing_page_path,
    argMax(device_category, OPEN_DATE) AS device_category,
    argMax(OPEN_DATE, OPEN_DATE) AS date_hour_minute
    FROM (
        SELECT toDate(OPEN_DATE) AS OPEN_DATE, DB_NAME AS db_name, NETWORK_USER_ID AS networkUserId, 
        GA_CID AS ga_cid, REG_COUNTRY AS country_iso_code, zone, source_medium, utm_content, 
        utm_campaign, landing_page_path, device_category, type
        FROM {REG_TABLE} WHERE type NOT IN  ('case1', 'mobile', 'test', 'bad domain') 
        AND toDate(OPEN_DATE) >= '{start_date_network}' AND toDate(OPEN_DATE) < '{start_date}'
    )
    GROUP BY networkUserId
     """

    result, columns = CONNECTION_CH.execute(select, with_column_types=True)
    df_networkUserId = pd.DataFrame(result, columns=[column[0] for column in columns])
    df_networkUserId.networkUserId = df_networkUserId.networkUserId.astype('int64')
    df_networkUserId['date_hour_minute'] = pd.to_datetime(df_networkUserId['date_hour_minute'], format='%Y-%m-%d %H:%M')
    df = pd.merge(df_reg, df_networkUserId, how='inner', left_on=['NETWORK_USER_ID'], right_on=['networkUserId']).drop(
        columns=['networkUserId', 'db_name'])
    df["reg_lag"] = abs((df.OPEN_DATE - df.date_hour_minute) / pd.Timedelta(minutes=1))
    df["type"] = "networkUserId"
    df["subtype"] = "networkUserId"
    df_final = pd.concat([df_final, df], ignore_index=True)
    if ENABLE_PRINT:
        print('\tdf_networkUserId:', df.shape)
    return df_final


def create_original_utm(x):
    """
    функция позволяет вернуть utm_campaign, когда utm_content NULL
    """
    if x["UTM_CONTENT"] == '' or x["UTM_CONTENT"] is None:
        return x["UTM_CAMPAIGN"]
    else:
        return x["UTM_CONTENT"]


def get_original_utm(df_final, df_reg):
    df = df_reg.query('not UTM_SOURCE.isnull()').copy()
    df['source_medium'] = df['UTM_SOURCE'] + ' / ' + df['UTM_MEDIUM']
    df['utm_campaign'] = df['UTM_CAMPAIGN']
    df['utm_content'] = df.apply(create_original_utm, axis=1)
    df['date_hour_minute'] = df['OPEN_DATE']
    df["type"] = "original_utm"
    df["subtype"] = "original_utm"
    df_final = pd.concat([df_final, df], ignore_index=True)
    if ENABLE_PRINT:
        print('\tdf_original_utm:', df.shape)
    return df_final


def get_case1(df_final, df_reg, df_ga, no_direct, reg_day):
    df = pd.DataFrame()  # сюда складываем готовый результат
    df_reg_temp = df_reg.copy()  # временная таблица рег
    df_visits_ga = df_ga.copy()
    df_visits_ga = df_visits_ga.sort_values(['date_hour_minute'])

    # начинаем подбор визитов с регами до тех пор, пока таблица рег не опустеет
    while len(df_reg_temp) != 0:
        df_reg_temp = df_reg_temp.sort_values(['OPEN_DATE'])
        df_reg_temp = pd.merge_asof(df_reg_temp, df_visits_ga,
                                    left_on='OPEN_DATE',
                                    right_on='date_hour_minute',
                                    left_by='REG_COUNTRY',
                                    right_by='country_iso_code',
                                    direction='backward')

        # оставим только те реги, которые нашлись
        df_temp = df_reg_temp.query('not date_hour_minute.isnull()').copy()
        df_temp["reg_lag"] = abs((df_temp.OPEN_DATE - df_temp.date_hour_minute) / pd.Timedelta(minutes=1))
        df_temp = df_temp.sort_values('reg_lag').drop_duplicates(['DB_NAME'], keep='first')
        if len(df_temp) == 0:  # будут случаи, когда осталось немного визитов с регами, но подобрать мы не можем
            break
        # пробуем, а сколько осталось не размеченными
        key_diff = set(df_visits_ga.key).difference(df_temp.key)
        where_diff = df_visits_ga.key.isin(key_diff)
        df_visits_ga = df_visits_ga[where_diff]

        df = pd.concat([df, df_temp], ignore_index=True)
        df_reg_temp = update_df_regs(df_reg_temp, df)  # уменьшим временную таблицу рег уже найденным результатом
        df_reg_temp = df_reg_temp.drop(columns=['date_hour_minute', 'country_iso_code', 'source_medium', 'utm_campaign',
                                                'utm_content', 'landing_page_path', 'device_category',
                                                'sessions', 'reg_goal', 'login_goal', 'ga_id', 'ga_cid', 'key'])
    df["type"] = "case1"
    df["subtype"] = f"case1_reg_goal_0_no_direct_{no_direct}"
    df_final = pd.concat([df_final, df], ignore_index=True)
    if ENABLE_PRINT:
        print(f'\tcase1:', df.shape)
    return df_final


def get_final_regs(df_final, df_reg):
    df = df_reg.copy()
    df["type"] = "no_match"
    df["subtype"] = "no_match"
    df["source_medium"] = "no_match"
    df["utm_campaign"] = "no_match"
    df["utm_content"] = "no_match"
    df_final = pd.concat([df_final, df], ignore_index=True)

    if ENABLE_PRINT:
        print('\tdf_no_match:', df.shape)
        print('\tdf_final:', df_final.shape)
    return df_final


def update_df_regs(df_reg, df_final):
    key_diff = set(df_reg.DB_NAME).difference(df_final.DB_NAME)
    where_diff = df_reg.DB_NAME.isin(key_diff)
    df_reg = df_reg[where_diff]
    return df_reg


def update_ga_visits(df_visit, df_final):
    key_diff = set(df_visit.key).difference(df_final.key)
    where_diff = df_visit.key.isin(key_diff)
    df_visit = df_visit[where_diff]
    return df_visit


def main():
    today = date.today()
    last_day = START_DAY

    while last_day > 0:
        yesterday = today - timedelta(days=last_day)
        # секция получения исходных данных
        print(f'Загружаем основные данные на {yesterday}:')
        df_reg = get_reg_sql(yesterday)
        if ENABLE_PRINT:
            print('\tdf_reg: ', df_reg.shape)

        df_visits_ga = get_ga_visits(yesterday)
        if ENABLE_PRINT:
            print('\tdf_visits_ga:', df_visits_ga.shape)

        df_bad_domain = get_bad_domain()
        if ENABLE_PRINT:
            print('\tdf_bad_domain:', df_bad_domain.shape)

        df_visits_ym = get_ym_visits(yesterday, last_day)
        if ENABLE_PRINT:
            print('\tdf_visits_ym:', df_visits_ym.shape)

        # Соберем мобильные реги
        df_final = pd.DataFrame()
        count = 1
        if ENABLE_PRINT:
            print(f'Начинаем объединять данные:\nЭтап {count} Мобильные реги')
        df_final = get_mobile_regs(df_final, df_reg)
        df_reg = update_df_regs(df_reg, df_final)
        count += 1
        if ENABLE_PRINT:
            print('\tdf_final:', df_final.shape)
            print('\tdf_reg:', df_reg.shape)

            # Соберем тестовые реги
            print(f'Этап {count} Тестовые реги\n\tdf_final:', df_final.shape)
        df_final = get_test_regs(df_final, df_reg)
        df_reg = update_df_regs(df_reg, df_final)
        count += 1
        if ENABLE_PRINT:
            print('\tdf_reg:', df_reg.shape)

            # Соберем реги с плохих доменов
            print(f'Этап {count} Реги с плохих доменов\n\tdf_final:', df_final.shape)
        df_final = get_bad_domain_regs(df_final, df_reg, df_bad_domain)
        df_reg = update_df_regs(df_reg, df_final)
        count += 1
        if ENABLE_PRINT:
            print('\tdf_reg:', df_reg.shape)

        # оставляем direct трафик или пытаемся найти последний непрямой визит
        # 0 - оставляем direct, 1 - ищем последний непрямой
        no_direct = 1

        while no_direct != -1:
            # YM_good_match
            # когда совпадает страна и client_id
            if ENABLE_PRINT:
                print(f'Этап {count} Good matches c YM c no_direct = {no_direct}\n\tdf_final:', df_final.shape)
            df_final = get_good_match(df_final, df_reg, df_visits_ym, "ym", no_direct)
            df_reg = update_df_regs(df_reg, df_final)
            count += 1
            if ENABLE_PRINT:
                print('\tdf_reg:', df_reg.shape)

                # GA_good_match
                # когда совпадает страна и client_id
                print(f'Этап {count} Good matches c GA c no_direct = {no_direct}\n\tdf_final:', df_final.shape)
            df_final = get_good_match(df_final, df_reg, df_visits_ga, "ga", no_direct)
            df_reg = update_df_regs(df_reg, df_final)
            count += 1
            if ENABLE_PRINT:
                print('\tdf_reg:', df_reg.shape)

                # YM_cid_match
                # когда совпадает только client_id, а цель на регу = 1
                print(f'Этап {count} CID matches YM  с reg_goal = 1 и no_direct = {no_direct}\n\tdf_final:',
                      df_final.shape)
            df_final = get_cid_match(df_final, df_reg, df_visits_ym, "ym", 1, no_direct)
            df_reg = update_df_regs(df_reg, df_final)
            count += 1
            if ENABLE_PRINT:
                print('\tdf_reg:', df_reg.shape)

                # GA_cid_match
                # когда совпадает только client_id, а цель на регу = 1
                print(f'Этап {count} CID matches GA с reg_goal = 1 и no_direct = {no_direct}\n\tdf_final:',
                      df_final.shape)
            df_final = get_cid_match(df_final, df_reg, df_visits_ga, "ga", 1, no_direct)
            df_reg = update_df_regs(df_reg, df_final)
            count += 1
            if ENABLE_PRINT:
                print('\tdf_reg:', df_reg.shape)

                # YM_cid_match
                # когда совпадает только client_id, а цель на регу = 0
                print(f'Этап {count} CID matches YM с reg_goal = 1 и no_direct = {no_direct}\n\tdf_final:',
                      df_final.shape)
            df_final = get_cid_match(df_final, df_reg, df_visits_ym, "ym", 0, no_direct)
            df_reg = update_df_regs(df_reg, df_final)
            count += 1
            if ENABLE_PRINT:
                print('\tdf_reg:', df_reg.shape)

                # GA_cid_match
                # когда совпадает только client_id, а цель на регу = 0
                print(f'Этап {count} CID matches GA с reg_goal = 0 и no_direct = {no_direct}\n\tdf_final:',
                      df_final.shape)
            df_final = get_cid_match(df_final, df_reg, df_visits_ga, "ga", 0, no_direct)
            df_reg = update_df_regs(df_reg, df_final)
            count += 1
            if ENABLE_PRINT:
                print('\tdf_reg:', df_reg.shape)

            # т.к мы проверяли только по последним непрямым заходам и нашли все что могли,
            # то теперь проверим еще раз все по прямым заходам
            # а если мы уже проверям по всем заходам, то просто выйдем
            no_direct -= 1

        # Проверим по networkUserId
        if len(df_reg) > 0:
            if ENABLE_PRINT:
                print(f'Этап {count} Проверка по networkUserId\n\tdf_final:', df_final.shape)
            df_final = get_networkUserId_regs(yesterday, df_final, df_reg)
            df_reg = update_df_regs(df_reg, df_final)
            count += 1
            if ENABLE_PRINT:
                print('\tdf_reg:', df_reg.shape)

        # Если есть UTM метка, будем доверять ей
        if len(df_reg) > 0:
            if ENABLE_PRINT:
                print(f'Этап {count} Проверка родной UTM метки\n\tdf_final:', df_final.shape)
            df_final = get_original_utm(df_final, df_reg)
            df_reg = update_df_regs(df_reg, df_final)
            count += 1

        # Подберем реги по ближайшему времени и стране
        if len(df_reg) > 0:
            if ENABLE_PRINT:
                print(f'Этап {count} Case1\n\tdf_final:', df_final.shape)
            # оставим только неиспользованные ga сессии
            df_visits_ga_not_used = update_ga_visits(df_visits_ga, df_final)
            df_final = get_case1(df_final, df_reg, df_visits_ga_not_used, no_direct, yesterday)
            df_reg = update_df_regs(df_reg, df_final)
            count += 1
            if ENABLE_PRINT:
                print('\tdf_reg:', df_reg.shape)

        # С оставшимися пока ничего делать не будем, обозначим как есть
        if len(df_reg) > 0:
            if ENABLE_PRINT:
                print(f'Этап {count} Оставшиеся реги добавим как есть\n\tdf_final:', df_final.shape)
            df_final = get_final_regs(df_final, df_reg)

        # посмотрим где у нас NULL и заменим на 0
        df_final[['sessions', 'reg_goal', 'login_goal', 'ga_id']] \
            = df_final[['sessions', 'reg_goal', 'login_goal', 'ga_id']] \
            .fillna(value=0)
        df_final = df_final.drop(columns=['country_iso_code', 'reg_lag', 'ga_cid', 'key'])
        df_final = df_final.astype({
            'XML_ID': 'int32',
            'NETWORK_USER_ID': 'int32',
            'sessions': 'int32',
            'reg_goal': 'int32',
            'ga_id': 'int64',
            'login_goal': 'int32'
        })

        if TO_FILE:
            df_final.to_csv(f'df_final_{yesterday}_no_direct_ym.csv', index=False)
        if TO_CH:
            CONNECTION_CH.execute(f"ALTER TABLE {REG_TABLE} DELETE WHERE  toDate(OPEN_DATE) = '{yesterday}'")
            CONNECTION_CH.insert_dataframe(f'INSERT INTO {REG_TABLE} VALUES', df_final)
            print(f'Успешно записали в CH данные на {yesterday}')

        last_day -= 1


if __name__ == '__main__':
    main()
